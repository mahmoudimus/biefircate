/*
 * Copyright (c) 2022 TK Chia
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the developer(s) nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
1 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <inttypes.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "stage2/stage2.h"
#include "stage2/pci.h"

#define EHCI_EECP_LEGACY	0x01
#define XHCI_XECP_LEGACY	0x01
#define XHCI_XECP_PROTO		0x02

/*
 * EHCI/XHCI Legacy Support Capability Register.
 *
 * NOTE:
 *
 *   * Benjamin David Lunt found (https://github.com/fysnet/FYSOS/blob/
 *     616ba687d48df3a42605dd7ede0936dfae57e878/main/usb/utils/gdevdesc/
 *     gd_xhci.c#L178) that the QEMU emulator does not handle reads from an
 *     XHCI controller's MMIO space that are not 32-bit aligned.  I have
 *     found that the problem persists even in QEMU 6.2.0.  Urgh.
 *
 *     Because of this, all reads & writes that may involve XHCI's MMIO space
 *     need to be made 32-bit aligned.  To do so, all bit fields in XHCI
 *     structures are bit fields of uint32_t.
 *
 *     Unfortunately, even with this, GCC makes it hard to guarantee that
 *     accesses are always 32-bit --- even with -fstrict-volatile-bitfields. 
 *     In particular it may read the XHCI PAGESIZE register's bit field with
 *     the `movzwl' (`movzx ..., word') instruction, a 16-bit access (!). 
 *     The only sure way to ensure a 32-bit read is to read an _entire_
 *     32-bit register each time into, say, a temporary variable.  Urrrgh.
 *
 *   * Some register fields are marked `RsvdZ' in the XHCI standard, which
 *     means that the fields are
 *	"[r]eserved for future RW1C [write-1-to-clear-status]
 *	 implementations.  Software shall use `0' for writes to these bits."
 *     We need to handle such fields with care, to avoid accidentally
 *     clobbering the wrong parts of the USB device state.
 *
 *     (The EHCI standard also indicates that some bits are "reserved and
 *     should be set to zero".)
 *
 *   * Many USB MMIO registers can be accessed as if they are normal memory.
 *     I only mark as `MMIO' (= `volatile') the registers which cannot be so
 *     treated.
 */
typedef PACKED_union {
	uint32_t W;
	PACKED_struct {
		uint32_t : 16,
			 BIOS_OWNED : 1, : 7,	/* HC "BIOS owned" semaphore */
			 OS_OWNED : 1, : 7;	/* HC "OS owned" semaphore */
	};
} usb_ehci_legacy_sup_t, usb_xhci_legacy_sup_t;

/* EHCI/XHCI Legacy Support Control and Status Register. */
typedef PACKED_union {
	uint32_t W;
	PACKED_struct {
		uint32_t SMI_EINT_ENA : 1, : 3,	/* USB SMI enable */
			 SMI_HSE_ENA : 1, : 3,	/* SMI on host error enable */
			 : 5, SMI_OS_ENA : 1,	/* SMI on OS ownership
						   enable */
			 SMI_PCI_ENA : 1,	/* SMI on PCI command enable */
			 SMI_BAR_ENA : 1,	/* SMI on BAR enable */
			 SMI_EINT : 1, : 3,	/* SMI on event interrupt */
			 SMI_HSE : 1,		/* SMI on host system error */
			 RsvdZ_1 : 3,		/* RsvdZ (see NOTE above) */
			 RsvdZ_2 : 5,
			 SMI_OS : 1,		/* SMI on OS ownership
						   change */
			 SMI_PCI : 1,		/* SMI on PCI command */
			 SMI_BAR : 1;		/* SMI on BAR */
	};
} usb_ehci_legacy_ctl_t, usb_xhci_legacy_ctl_t;

/* EHCI Structural Parameters. */
typedef PACKED_union {
	uint32_t W;
	PACKED_struct {
		uint8_t N_PORTS : 4,	/* no. of phy. downstream ports */
			PPC : 1, : 2,	/* port power control */
			PRR : 1;	/* port routing rules */
		uint8_t N_PCC : 4,	/* no. of oprts per companion ctrlr. */
			N_CC : 4;	/* no. of companion controllers */
		uint8_t P_INDICATOR : 1,/* port indicators */
			: 3,
			DBG_PORT : 4;	/* debug port no. */
		uint8_t : 8;
	};
} usb_ehci_hcsparams_t;

/* EHCI Capability Parameters. */
typedef PACKED_union {
	uint32_t W;
	PACKED_struct {
		uint8_t AC64 : 1,	/* 64-bit addressing */
			PFL : 1,	/* programmable frame list */
			ASP : 1, : 1,	/* asynchronous schedule park */
			ISOCH_THR : 4;	/* isochronous scheduling threshold */
		uint8_t EECP;		/* extended capabilities pointer */
		uint16_t : 16;		/* reserved */
	};
} usb_ehci_hccparams_t;

/* EHCI Host Controller Capability Registers. */
typedef MMIO_struct {
	uint8_t CAPLENGTH;		/* capability registers length */
	uint8_t : 8;			/* reserved */
	uint16_t HCIVERSION;		/* interface version number */
	usb_ehci_hcsparams_t HCSPARAMS;	/* structural parameters */
	usb_ehci_hccparams_t HCCPARAMS;	/* capability parameters */
	uint32_t HCSP_PORTROUTE;	/* companion port route description */
} usb_ehci_t;

/* EHCI USB Command Register. */
typedef PACKED_union {
	uint32_t W;
	PACKED_struct {
		uint32_t RS : 1,	/* run/stop */
			 HCRST : 1,	/* HC reset */
			 FLSTSZ : 2,	/* frame list size */
			 PSE : 1,	/* periodic schedule enable */
			 ASE : 1,	/* asynchronous schedule enable */
			 IAADB : 1,	/* intr. on async. advance doorbell */
			 LHCRST : 1,	/* light HC reset */
			 ASPMC : 2,	/* async. schedule park mode count */
			 RsvdZ_1 : 1,	/* RsvdZ (?) (see NOTE above) */
			 ASPME : 1,	/* async. schedule park mode enable */
			 RsvdZ_2 : 4,
			 INTR_THR : 8,	/* interrupt threshold control */
			 RsvdZ_3 : 8;
	};
} usb_ehci_usbcmd_t;

/* EHCI USB Status Register. */
typedef PACKED_union {
	uint32_t W;
	PACKED_struct {
		uint32_t USBINT : 1,	/* USB interrupt */
			 USBERRINT : 1,	/* USB error interrupt */
			 PCD : 1,	/* port change detect */
			 FLSTROLL : 1,	/* frame list rollover */
			 HSE : 1,	/* host system error */
			 IAA : 1,	/* interrupt on async. advance */
			 RsvdZ_1 : 6,	/* RsvdZ (?) (see NOTE above) */
			 HCH : 1,	/* HC halted */
			 RECLAIM : 1,	/* reclamation */
			 PSSTS : 1,	/* periodic schedule status */
			 ASSTS : 1,	/* asynchronous schedule status */
			 RsvdZ_2 : 16;
	};
} usb_ehci_usbsts_t;

/* EHCI Host Controller Operational Registers. */
typedef MMIO_struct {
	MMIO usb_ehci_usbcmd_t USBCMD;	/* USB command */
	MMIO usb_ehci_usbsts_t USBSTS;	/* USB status */
} usb_ehci_op_t;

/* EHCI Extended Capability. */
typedef PACKED_union {
	uint32_t W;
	PACKED_struct {
		uint8_t CAPID;		/* capability id. */
		uint8_t NXT;		/* next capability pointer */
	};
	MMIO usb_ehci_legacy_sup_t LEG;
} usb_ehci_eec_t;

/* XHCI Structural Parameters 1. */
typedef PACKED_union {
	uint32_t W;
	PACKED_struct {
		uint32_t MAXSLOTS : 8,		/* no. of device slots */
			 MAXINTRS : 11, : 5,	/* no. of interrupters */
			 MAXPORTS : 8;		/* no. of ports */
	};
} usb_xhci_hcsparams1_t;

/* XHCI Host Controller Capability 1 Parameters. */
typedef PACKED_union {
	uint32_t W;
	PACKED_struct {
		uint32_t AC64 : 1,	/* 64-bit addressing */
			 BNC : 1,	/* bandwidth negotiation */
			 CSZ : 1,	/* context size */
			 PPC : 1,	/* port power control */
			 PIND : 1,	/* port indicators */
			 LHRC : 1,	/* light HC reset */
			 LTC : 1,	/* latency tolerance messaging */
			 NSS : 1,	/* no secondary stream id. support */
			 PAE : 1,	/* parse all event data */
			 SPC : 1,	/* stopped-short packet */
			 SEC : 1,	/* stopped EDTLA capability */
			 CFC : 1,	/* contiguous frame */
			 MAXPSASIZE : 4,/* max. primary stream array size */
			 XECP : 16;	/* extended capabilities pointer */
	};
} usb_xhci_hccparams1_t;

/* XHCI Host Controller Capability Registers. */
typedef PACKED_struct {
	uint32_t CAPLENGTH : 8,		/* capability registers length */
		 : 8,			/* reserved */
		 HCIVERSION : 16;	/* interface version number */
					/* structural parameters 1, 2, 3 */
	usb_xhci_hcsparams1_t HCSPARAMS1;
	uint32_t HCSPARAMS2, HCSPARAMS3;
					/* capability parameters 1 */
	usb_xhci_hccparams1_t HCCPARAMS1;
	uint32_t DBOFF;			/* doorbell offset */
	uint32_t RTSOFF;		/* runtime register space offset */
	uint32_t HCCPARAMS2;		/* capability parameters 2 */
} usb_xhci_t;

/* XHCI Supported Protocol Capability. */
typedef PACKED_struct {
	PACKED_union {
		PACKED_struct {
			uint32_t : 16,
				 REVMIN : 8,	/* minor revision */
				 REVMAJ : 8;	/* major revision */
		};
		/* For detecting QEMU lossage... */
		MMIO_struct {
			uint16_t : 16;
			uint8_t REVMIN_B;	/* minor rev. (byte access) */
			uint8_t REVMAJ_B;	/* major rev. (byte access) */
		};
	};
	uint32_t NAME;			/* name string */
	uint32_t PORTOFF : 8,		/* compatible port offset */
		 NPORTS : 8,		/* compatible port count */
		 : 1, HSO : 1,		/* high-speed only (USB 2) */
		 IHI : 1,		/* integrated hub implem'd (USB 2) */
		 HLC : 1,		/* hardware LPM capability (USB 2) */
		 BLC : 1,		/* BESL LPM capability (USB 2) */
		 : 3,
		 LSECC : 1,		/* link soft err. count cap. (USB 3) */
		 MHD : 3,		/* hub depth (USB 2, USB 3) */
		 PSIC : 4;		/* protocol speed id. count */
	uint32_t SLOTTYP : 5, : 3,	/* protocol slot type */
		 : 24;
} usb_xhci_proto_t;

/* XHCI Extended Capability. */
typedef MMIO_union {
	PACKED_struct {
		uint32_t CAPID : 8,	/* capability id. */
			 NXT : 8;	/* next capability pointer */
	};
	PACKED_struct {
		MMIO usb_xhci_legacy_sup_t USBLEGSUP;
		MMIO usb_xhci_legacy_ctl_t USBLEGCTLSTS;
	} LEG;
	usb_xhci_proto_t PROTO;
} usb_xhci_xec_t;

/* XHCI USB Command Register. */
typedef PACKED_union {
	uint32_t W;
	PACKED_struct {
		uint32_t RS : 1,	/* run/stop */
			 HCRST : 1,	/* HC reset */
			 INTE : 1,	/* interrupter enable */
			 HSEE : 1,	/* host system error enable */
			 : 3,
			 LHCRST : 1,	/* light HC reset */
			 CSS : 1,	/* controller save state */
			 CRS : 1,	/* controller restore state */
			 EWE : 1,	/* enable wrap event */
			 EU3S : 1,	/* enable U3 MFINDEX stop */
			 : 1,
			 CME : 1,	/* CEM enable */
			 ETE : 1,	/* extended xfer. burst count enable */
			 TSC_EN : 1,	/* extended TBC TRB status enable */
			 VTIOE : 1,	/* VTIO enable */
			 : 15;
	};
} usb_xhci_usbcmd_t;

/* XHCI Page Size Register. */
typedef PACKED_struct {
	uint32_t P : 16,		/* page size divided by 4 KiB */
		 : 16;
} usb_xhci_pagesize_t;

/* XHCI USB Status Register. */
typedef PACKED_union {
	uint32_t W;
	PACKED_struct {
		uint32_t HCH : 1,	/* HC halted */
			 : 1,
			 HSE : 1,	/* host system error */
			 EINT : 1,	/* event interrupt */
			 PCD : 1,	/* port change detect */
			 RsvdZ_1 : 3,	/* RsvdZ (see NOTE above) */
			 SSS : 1,	/* save state status */
			 RSS : 1,	/* restore state status */
			 SRE : 1,	/* save/restore error */
			 CNR : 1,	/* controller not ready */
			 HCE : 1,	/* host controller error */
			 RsvdZ_2 : 19;
	};
} usb_xhci_usbsts_t;

/* XHCI Host Controller Operational Registers. */
typedef MMIO_struct {
	MMIO usb_xhci_usbcmd_t USBCMD;	/* USB command */
	MMIO usb_xhci_usbsts_t USBSTS;	/* USB status */
	MMIO usb_xhci_pagesize_t PAGESIZE; /* page size */
} usb_xhci_op_t;

static void ehci_stop_legacy(uint32_t locn, usb_ehci_hccparams_t hccp)
{
	uint8_t off = hccp.EECP;
	while (off >= 0x40) {
		usb_ehci_eec_t cap1;
		cap1.W = in_pci_d(locn, off);
		if (cap1.CAPID == EHCI_EECP_LEGACY) {
			usb_ehci_legacy_ctl_t cap2;
			cap2.W = in_pci_d(locn, off + 4);
			cprintf("  USBLEGSUP: 0x%08" PRIx32 " "
				  "@ +0x%02" PRIx8 "  "
				  "USBLEGCTLSTS: 0x%08" PRIx32 "\n",
			    cap1.W, off, cap2.W);
			if (cap1.LEG.OS_OWNED)
				return;
			cap1.LEG.OS_OWNED = 1;
			out_pci_d(locn, off, cap1.W);
			do {
				yield_to_irq();
				cap1.W = in_pci_d(locn, off);
			} while (cap1.LEG.BIOS_OWNED);
			cprintf("           " ARROW_RIGHT_STR
					      " 0x%08" PRIx32 "\n", cap1.W);
			return;
		}
		off = cap1.NXT;
	}
}

static void ehci_init_bus(bdat_pci_dev_t *pd)
{
	uint32_t locn = pd->pci_locn;
	usb_ehci_hcsparams_t hcsp;
	usb_ehci_hccparams_t hccp;
	usb_ehci_t *hc;
	uint64_t hc_pa;
	unsigned seg = locn >> 16, bus = (locn >> 8) & 0xff,
		 dev = (locn >> 3) & 0x1f, fn = locn & 7;
	int8_t irq = (int8_t)in_pci_d(locn, 0x3c);
	hc = pci_va_map(locn, 0, 0x200, &hc_pa);
	cprintf("USB EHCI @ %04x:%02x:%02x.%x  "
		"USBBASE: @0x%" PRIx32 "%08" PRIx32 "  IRQ: %" PRId8 "\n",
	    seg, bus, dev, fn, (uint32_t)(hc_pa >> 32), (uint32_t)hc_pa, irq);
	hcsp = hc->HCSPARAMS;
	hccp = hc->HCCPARAMS;
	cprintf("  CAPLENGTH: 0x%" PRIx8 "  HCIVERSION: 0x%" PRIx16 "  "
		  "HCSPARAMS: 0x%" PRIx32 "  HCCPARAMS: 0x%" PRIx32 "\n",
	    hc->CAPLENGTH, hc->HCIVERSION, hcsp.W, hccp.W);
	ehci_stop_legacy(locn, hccp);
	mem_va_unmap(hc, 0x200);
}

static void xhci_stop_legacy(uint64_t hc_pa, usb_xhci_hccparams1_t hccp1)
{
	uint64_t pa = hc_pa;
	uint16_t off = hccp1.XECP;
	while (off != 0) {
		usb_xhci_xec_t *xec;
		pa += (uint64_t)off * 4;
		xec = mem_va_map(pa, sizeof(usb_xhci_xec_t), PTE_CD);
		if (xec->CAPID == XHCI_XECP_LEGACY) {
			usb_xhci_legacy_sup_t cap1 = xec->LEG.USBLEGSUP;
			usb_xhci_legacy_ctl_t cap2 = xec->LEG.USBLEGCTLSTS;
			cprintf("  USBLEGSUP: 0x%08" PRIx32 " "
				  "@ 0x%" PRIx32 "%08" PRIx32 "  "
				  "USBLEGCTLSTS: 0x%08" PRIx32 "\n",
			    cap1.W, (uint32_t)(pa >> 32), (uint32_t)pa,
			    cap2.W);
			if (cap1.OS_OWNED)
				return;
			cap1.OS_OWNED = 1;
			xec->LEG.USBLEGSUP = cap1;
			do {
				yield_to_irq();
				cap1 = xec->LEG.USBLEGSUP;
			} while (cap1.BIOS_OWNED);
			cprintf("           " ARROW_RIGHT_STR
					      " 0x%08" PRIx32 "\n", cap1.W);
			return;
		}
		off = xec->NXT;
		mem_va_unmap(xec, sizeof(usb_xhci_xec_t));
	}
}

static bool is_power_of_2(uint16_t x)
{
	return x != 0 && (x & (x - 1)) == 0;
}

static void xhci_start_ports(uint64_t hc_pa, usb_xhci_op_t *cop,
			     usb_xhci_hcsparams1_t hcsp1,
			     usb_xhci_hccparams1_t hccp1)
{
	uint64_t pa = hc_pa;
	uint16_t off = hccp1.XECP;
	uint8_t cap_id;
	usb_xhci_pagesize_t cpszr = cop->PAGESIZE;
	uint16_t cpszv = cpszr.P;
	bool qemu_weird = false;
	if (!is_power_of_2(cpszr.P)) {
		warn("PAGESIZE not a single power of 2: 0x%" PRIx16, cpszv);
		return;
	}
	cprintf("  PAGESIZE: 0x%" PRIx16 " (* 4 KiB)\n"
		"  max. ports: %" PRIu8 "  max. intrs: %" PRIu16 "  "
		  "max. slots: %" PRIu8 "\n",
	    cpszv, hcsp1.MAXPORTS, (uint16_t)hcsp1.MAXINTRS, hcsp1.MAXSLOTS);
	while (off != 0) {
		usb_xhci_xec_t *xec;
		pa += (uint64_t)off * 4;
		xec = mem_va_map(pa, sizeof(usb_xhci_xec_t), PTE_CD);
		cap_id = xec->CAPID;
		if (cap_id == XHCI_XECP_PROTO) {
			usb_xhci_proto_t proto = xec->PROTO;
			uint8_t port_off = proto.PORTOFF,
				n_ports = proto.NPORTS,
				rev_maj = proto.REVMAJ,
				rev_min = proto.REVMIN;
			uint32_t proto_name = proto.NAME;
			if (rev_maj != xec->PROTO.REVMAJ_B ||
			    rev_min != xec->PROTO.REVMIN_B) {
				if (!qemu_weird) {
					qemu_weird = true;
					warn("working around broken XHCI "
					     "implem. (QEMU?)");
				}
			}
			cprintf("  ports %" PRIu8 "--%" PRIu8 "  proto.: "
					"%c%c%c%c %02" PRIx8 ".%02" PRIx8 "\n",
			    port_off, port_off + n_ports - 1,
			    (char)proto_name, (char)(proto_name >> 8),
			    (char)(proto_name >> 16), (char)(proto_name >> 24),
			    rev_maj, rev_min);
		}
		off = xec->NXT;
		mem_va_unmap(xec, sizeof(usb_xhci_xec_t));
	}
}

static void xhci_init_bus(bdat_pci_dev_t *pd)
{
	uint32_t locn = pd->pci_locn;
	usb_xhci_hcsparams1_t hcsp1;
	usb_xhci_hccparams1_t hccp1;
	usb_xhci_t *hc;
	usb_xhci_op_t *cop;
	uint64_t hc_pa, cop_pa;
	uint8_t cap_len;
	unsigned seg = locn >> 16, bus = (locn >> 8) & 0xff,
		 dev = (locn >> 3) & 0x1f, fn = locn & 7;
	int8_t irq = (int8_t)in_pci_d(locn, 0x3c);
	hc = pci_va_map(locn, 0, sizeof(usb_xhci_t), &hc_pa);
	cprintf("USB XHCI @ %04x:%02x:%02x.%x  "
		"BASE: @0x%" PRIx32 "%08" PRIx32 "  IRQ: %" PRId8 "\n",
	    seg, bus, dev, fn, (uint32_t)(hc_pa >> 32), (uint32_t)hc_pa, irq);
	cap_len = hc->CAPLENGTH;
	hcsp1 = hc->HCSPARAMS1;
	hccp1 = hc->HCCPARAMS1;
	cprintf("  CAPLENGTH: 0x%" PRIx8 "  HCIVERSION: 0x%" PRIx16 "\n"
		"  HCSPARAMS: 0x%" PRIx32 " 0x%" PRIx32 " 0x%" PRIx32 "  "
		  "HCCPARAMS: 0x%" PRIx32 " 0x%" PRIx32 "\n",
	    cap_len, hc->HCIVERSION,
	    hcsp1.W, hc->HCSPARAMS2, hc->HCSPARAMS3,
	    hccp1.W, hc->HCCPARAMS2);
	cop_pa = hc_pa + cap_len;
	cop = mem_va_map(cop_pa, sizeof(usb_xhci_op_t), PTE_CD);
	xhci_stop_legacy(hc_pa, hccp1);
	xhci_start_ports(hc_pa, cop, hcsp1, hccp1);
	mem_va_unmap(hc, sizeof(usb_xhci_t));
}

void usb_init(bparm_t *bparms)
{
	bparm_t *bp;
	for (bp = bparms; bp; bp = bp->next) {
		bdat_pci_dev_t *pd;
		if (bp->type != BP_PCID)
			continue;
		pd = &bp->u->pci_dev;
		switch (pd->class_if) {
		    case PCI_CIF_BUS_USB_EHCI:
			ehci_init_bus(pd);
			break;
		    case PCI_CIF_BUS_USB_XHCI:
			xhci_init_bus(pd);
			break;
		    default:
			;
		}
	}
}
