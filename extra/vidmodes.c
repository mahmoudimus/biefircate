/*
 * Copyright (c) 2022 TK Chia
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the developer(s) nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define GNU_EFI_USE_MS_ABI

#include <efi.h>
#include <efilib.h>
#include <stdbool.h>
#include <string.h>

extern EFI_GUID gEfiGraphicsOutputProtocolGuid;

static void wait_and_exit(EFI_STATUS status)
{
	Output(u"press a key to exit\r\n");
	WaitForSingleEvent(ST->ConIn->WaitForKey, 0);
	Exit(status, 0, NULL);
}

static void error_with_status(IN CONST CHAR16 *msg, EFI_STATUS status)
{
	Print(u"error: %s: %d\r\n", msg, (INT32)status);
	wait_and_exit(status);
}

static void list_vid_modes(void)
{
	EFI_GRAPHICS_OUTPUT_PROTOCOL *gop;
	EFI_GRAPHICS_OUTPUT_MODE_INFORMATION *mi;
	UINTN mi_sz;
	UINT32 curr_mode, max_mode, mode, hres, vres;
	unsigned cnt;
	EFI_STATUS status = LibLocateProtocol(&gEfiGraphicsOutputProtocolGuid,
	    (void **)&gop);
	if (EFI_ERROR(status))
		error_with_status(u"no GOP found", status);
	curr_mode = gop->Mode->Mode;
	max_mode = gop->Mode->MaxMode;
	Print(u"current mode: 0x%04x  max. mode: 0x%04x\r\n"
	       "RGBX8888 modes:", curr_mode, max_mode - 1);
	for (mode = 0, cnt = 0; mode < max_mode; ++mode) {
		status = gop->QueryMode(gop, mode, &mi_sz, &mi);
		if (EFI_ERROR(status))
			continue;
		if (mi_sz < sizeof(*mi) ||
		    mi->PixelFormat != PixelRedGreenBlueReserved8BitPerColor)
			continue;
		if (cnt % 4 == 0)
			Output(u"\r\n");
		++cnt;
		hres = mi->HorizontalResolution;
		vres = mi->VerticalResolution;
		Print(u"  0x%04x: %4u%c*%4u", mode, hres,
		    hres == mi->PixelsPerScanLine ? u' ' : u'+', vres);
	}
	if (cnt == 0)
		Output(u" none");
	Output(u"\r\n"
		"BGRX8888 modes:");
	for (mode = 0, cnt = 0; mode < max_mode; ++mode) {
		status = gop->QueryMode(gop, mode, &mi_sz, &mi);
		if (EFI_ERROR(status))
			continue;
		if (mi_sz < sizeof(*mi) ||
		    mi->PixelFormat != PixelBlueGreenRedReserved8BitPerColor)
			continue;
		if (cnt % 4 == 0)
			Output(u"\r\n");
		++cnt;
		hres = mi->HorizontalResolution;
		vres = mi->VerticalResolution;
		Print(u"  0x%04x: %4u%c*%4u", mode, hres,
		    hres == mi->PixelsPerScanLine ? u' ' : u'+', vres);
	}
	if (cnt == 0)
		Output(u" none");
	Output(u"\r\n"
		"other modes:");
	for (mode = 0, cnt = 0; mode < max_mode; ++mode) {
		status = gop->QueryMode(gop, mode, &mi_sz, &mi);
		if (EFI_ERROR(status))
			continue;
		if (mi_sz < sizeof(*mi) ||
		    mi->PixelFormat == PixelRedGreenBlueReserved8BitPerColor ||
		    mi->PixelFormat == PixelBlueGreenRedReserved8BitPerColor)
			continue;
		++cnt;
		hres = mi->HorizontalResolution;
		vres = mi->VerticalResolution;
		Print(u"\r\n  0x%04x: %4u{%u}*%4u  ", mode, hres,
		    mi->PixelsPerScanLine, vres);
		switch (mi->PixelFormat) {
		    case PixelBitMask:
			Print(u"RGBX 0x%x 0x%x 0x%x 0x%x",
			    mi->PixelInformation.RedMask,
			    mi->PixelInformation.GreenMask,
			    mi->PixelInformation.BlueMask,
			    mi->PixelInformation.ReservedMask);
			break;
		    case PixelBltOnly:
			Output(u"blt only");
			break;
		    default:
			Print(u"unknown format 0x%x", (UINT32)mi->PixelFormat);
		}
	}
	if (cnt == 0)
		Output(u" none");
	Output(u"\r\n");
}

EFI_STATUS efi_main(EFI_HANDLE image_handle, EFI_SYSTEM_TABLE *system_table)
{
	InitializeLib(image_handle, system_table);
	Output(u".:. Video mode lister " PACKAGE_VERSION " .:.\r\n");
	list_vid_modes();
	wait_and_exit(0);
	return 0;
}
